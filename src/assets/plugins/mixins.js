import store from '@/store'

export function getImgUrl(url, min) {
  let ifMin = ''
  if (min) {
    ifMin = 'min/'
  }
  if (url.startsWith('http')) {
    return url
  } else {
    return `${store.state.apiUrls.api}/${ifMin}${url}`
  }
}