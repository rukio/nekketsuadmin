import Vue from 'vue'
import VueRouter from 'vue-router'
//***
// components
import Index from '@/components/pages/Index'
import IndexPageSettings from '@/components/pages/PageSettings'
import UserManager from '@/components/pages/UserManager'
import Auth from '@/components/auth/Authorization'

// News
import News from '@/components/pages/news/Index'
import NewsCreate from '@/components/pages/news/Create'
import NewsEdit from '@/components/pages/news/Edit'
import NewsPageSettings from '@/components/pages/news/PageSettings'

// Videos
import Videos from '@/components/pages/videos/Index'
import VideosCreate from '@/components/pages/videos/Create'
import VideosCreateCategory from '@/components/pages/videos/CreateCategory'
import VideosEdit from '@/components/pages/videos/Edit'
import VideosEditCategory from '@/components/pages/videos/EditCategory'
import VideosPageSettings from '@/components/pages/videos/PageSettings'

// Hall of Fame
import HallOfFame from '@/components/pages/hall_of_fame/Index'
import HallOfFameCreate from '@/components/pages/hall_of_fame/Create'
import HallOfFameEdit from '@/components/pages/hall_of_fame/Edit'
import HallOfFamePageSettings from '@/components/pages/hall_of_fame/PageSettings'
import Registration from '@/components/auth/Registration'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Index, name: 'index' },
    { path: '/page-settings', component: IndexPageSettings, name: 'index-page-settings' },
    { path: '/user-mng', component: UserManager, name: 'user-manager' },
    { path: '/login', component: Auth, name: 'login' },
    { path: '/register', component: Registration, name: 'registration' },
    
    // News
    { path: '/news', component: News, name: 'news-index'},
    { path: '/news/create', component: NewsCreate, name: 'news-create'},
    { path: '/news/edit/:id', component: NewsEdit, name: 'news-edit'},
    { path: '/news/page-settings', component: NewsPageSettings, name: 'news-page-settings'},

    // Videos
    { path: '/videos', component: Videos, name: 'videos-index'},
    { path: '/videos/create', component: VideosCreate, name: 'videos-create'},
    { path: '/videos/create-category', component: VideosCreateCategory, name: 'videos-create-category'},
    { path: '/videos/edit/:id', component: VideosEdit, name: 'videos-edit'},
    { path: '/videos/edit-category/:id', component: VideosEditCategory, name: 'videos-edit-category'},
    { path: '/videos/page-settings', component: VideosPageSettings, name: 'videos-page-settings'},

    // Hall of Fame
    { path: '/hall_of_fame', component: HallOfFame, name: 'hallOfFame-index'},
    { path: '/hall_of_fame/create', component: HallOfFameCreate, name: 'hallOfFame-create'},
    { path: '/hall_of_fame/edit/:id', component: HallOfFameEdit, name: 'hallOfFame-edit'},
    { path: '/hall_of_fame/page-settings', component: HallOfFamePageSettings, name: 'hallOfFame-page-settings'},
  ]
})
