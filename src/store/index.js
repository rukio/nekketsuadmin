import Vue from 'vue'
import Vuex from 'vuex'

import ContentControl from '@/store/modules/ContentControl'
import Sounds from '@/store/modules/Sounds'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    apiUrls: {
      api: 'http://api.nst.rip',
      domain: 'http://nst.rip',
      index: '/contents',
      
      // News
      news: '/contents',
      newsCount: '/contents/count',
      newsPage: '/contents',

      // Videos
      videos: '/contents',
      videosCount: '/contents/count',
      videosPage: '/contents',
      videosCategories: '/contents',

      // Hall of Fame
      hallOfFame: '/hallOfFame',
      hallOfFameCount: '/hallOfFame/count',
      hallOfFamePage: '/contents',

      chat: '/chat',
      uploadFile: '/uploadFile',
      uploadFileEditor: '/uploadFileEditor',
      login: '/auth/adminPanel',
      register: '/register',
      findUserByNickname: '/findUserByNickname',
      editUserByNickname: '/editUserByNickname',
      userCurrent: '/userCurrent'
    },
    apiIds: {
      hallOfFame: 98,
      news: 1,
      index: 96,
      videos: 100,
    },
    videoUrlComp: 'img.youtube.com/vi/',
    pageTitlePrefix: ' | Nekketsu Sports Tournaments',
    bgImageName: 'nkd-bg-mountain.jpg',
    bgColor: 'rgba(24, 60, 92, .6)',
    tokenKeyName: 'DefaultImageResolutionCode',
    profile: {}
  },
  mutations: {
    setBgImageName(state, value) {
      state.bgImageName = value
    },
    setBgColor(state, value) {
      state.bgColor = value
    },
    setProfile(state, value) {
      state.profile = value
    }
  },
  getters: {
    getCurrentTimezoneDate: () => ({date, type}) => {
      if (date) {
          let b = date.split(/\D+/);
          let dateResult = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]))
          dateResult.setHours(dateResult.getHours() + 3)

          if (type === 'date') {
              return dateResult.toISOString().split('T')[0].split('-').reverse().join('.')
          } else if (type === 'time') {
              return dateResult.toISOString().split('T')[1].split('.')[0]
          } else if (typeof type === 'undefined') {
              return `${dateResult.toISOString().split('T')[0].split('-').reverse().join('.')} ${dateResult.toISOString().split('T')[1].split('.')[0]}`
          }
      }
  },
  //Транслитерация кириллицы в URL
  urlRusLat: () => ({str}) => {
    str = str.toLowerCase(); // все в нижний регистр
      var cyr2latChars = new Array(
          ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
          ['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
          ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
          ['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
          ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
          ['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
          ['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],
          
          ['А', 'A'], ['Б', 'B'],  ['В', 'V'], ['Г', 'G'],
          ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'],  ['Ж', 'ZH'], ['З', 'Z'],
          ['И', 'I'], ['Й', 'Y'],  ['К', 'K'], ['Л', 'L'],
          ['М', 'M'], ['Н', 'N'], ['О', 'O'],  ['П', 'P'],  ['Р', 'R'],
          ['С', 'S'], ['Т', 'T'],  ['У', 'U'], ['Ф', 'F'],
          ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
          ['Ъ', ''],  ['Ы', 'Y'],
          ['Ь', ''],
          ['Э', 'E'],
          ['Ю', 'YU'],
          ['Я', 'YA'],
          
          ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
          ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
          ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
          ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
          ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
          ['z', 'z'],
          
          ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'],['E', 'E'],
          ['F', 'F'],['G', 'G'],['H', 'H'],['I', 'I'],['J', 'J'],['K', 'K'],
          ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'],['P', 'P'],
          ['Q', 'Q'],['R', 'R'],['S', 'S'],['T', 'T'],['U', 'U'],['V', 'V'],
          ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],
          
          [' ', '_'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
          ['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
          ['-', '-']

      );

      var newStr = new String();

      for (var i = 0; i < str.length; i++) {

          ch = str.charAt(i);
          var newCh = '';

          for (var j = 0; j < cyr2latChars.length; j++) {
              if (ch == cyr2latChars[j][0]) {
                  newCh = cyr2latChars[j][1];

              }
          }
          // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
          newStr += newCh;

      }
      // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
      // Так же удаляем символы перевода строки, но это наверное уже лишнее
      return newStr.replace(/[_]{2,}/gim, '_').replace(/\n/gim, '');
    },
    getImgUrl: (state) => ({url, min}) => {
      let ifMin = ''
      if (min) {
        ifMin = 'min/'
      }
      if (url.startsWith('http')) {
        return url
      } else {
        return `${state.apiUrls.api}/${ifMin}${url}`
      }
    }
  },
  modules: {
    ContentControl,
    Sounds
  }
})

export default store
