import Vue from 'vue'
import router from '@/router'

const state = {
  contentData: [],
  fields: {},
  fieldsEditReady: false,
  requestUrl: { get: '', put: '', post: '', delete: '', parent_id: 0 },
  notification: { message: '', visible: false, isAnimPlaying: false, duration: 0 },
  notificationTimeout: null,
  forDelete: '',
  paginationQuery: '',
  pageItemLimit: 10,
  currentPage: 1,
  contentTotal: 0,
  searchQuery: '',
  sortQuery: '',
  nestedEditingNow: { parentField: '', index: null, editing: false },
  newNestedItem: { uploadState: {} },
  charsForRandomUrl: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
}

const mutations = {
  setNestedEditingNow(state, value) {
    if (value.type === 'remove') {
      state.nestedEditingNow.parentField = ''
      state.nestedEditingNow.index = null
      state.nestedEditingNow.editing = false
    } else if (value.type === 'set') {
      state.nestedEditingNow.parentField = value.parentField
      state.nestedEditingNow.index = value.index
      state.nestedEditingNow.editing = value.editing
    } else if (value.type === 'removeEditing') {
      state.nestedEditingNow.editing = false
    }
  },
  setNestedNewCreating(state, data) {
    Vue.set(state.fields[data.fieldName], 'creating', data.value)
    for (let field in state.fields) {
      if (field !== data.fieldName) {
        Vue.set(state.fields[field], 'creating', false)
      }
    }
  },
  setFieldsEditReady(state, value) {
    state.fieldsEditReady = value
  },
  setNewNestedItem(state, value) {
    if (value.type === 'assign') {
      if (value.uploadState) {
        Vue.set(state.newNestedItem.uploadState, value.key, value.uploadState)
      }
      state.newNestedItem[value.key] = value.data
    } else if (value.type === 'stateUpdate') {
      Vue.set(state.newNestedItem.uploadState, value.key, value.data)
    } else if (value.type === 'remove') {
      state.newNestedItem = {uploadState: {}}
    }
  },
  setSearchQuery(state, value) {
    if (value) {
      state.searchQuery = `?search=${value}`
    } else {
      state.searchQuery = ''
    }
  },
  setContentData(state, value) {
    state.contentData = value
  },
  setFields(state, data) {
    if (typeof data !== 'string') {
      state.fields = data
    } else {
      let fields = {}
      for (let field of data.fieldsList.split(' ')) {
        fields[field] = { value: '', error: false }
      }
      state.fields = fields
      console.log(state.fields)
    }
  },
  setField(state, data) {
    if (!data.fieldName) {
      // state.fields[data.name] = { value: data.value, error: data.error || false }
      Vue.set(state.fields[data.name], 'value', data.value)
    } else {
      // state.fields[data.name] = { [data.fieldName]: data.value, error: data.error || false }
      Vue.set(state.fields[data.name], [data.fieldName], data.value)
    }
  },
  setRequestUrl(state, value) {
    let data = value
    if (!value.put) {
      data.put = value.get
    }
    if (!value.post) {
      if (!value.get) {
        data.post = value.put
      } else {
        data.post = value.get.split('/')[1].split('/')[0]
      }
    }
    state.requestUrl = data
  },
  setNotification(state, value) {
    state.notification = value
  },
  setNotificationMessage(state, value) {
    state.notification.message = value
  },
  setNotificationParam(state, value) {
    state.notification[value.name] = value.value
  },
  setNotificationTimeout(state, value) {
    if (value == 'clear') clearTimeout(state.notificationTimeout)
    else state.notificationTimeout = value
  },
  setPageItemLimit(state, value) {
    state.pageItemLimit = value
  },
  setCurrentPage(state, value) {
    state.currentPage = value
  },
  setContentTotal(state, value) {
    state.contentTotal = value
  },
  setSortQuery(state, value) {
    let queryResult = ''
    if (value.orderBy) {
      queryResult += `&orderBy=${value.orderBy}`
    }
    if (value.sortBy) {
      queryResult ? queryResult += `&sortBy=${value.sortBy}` : queryResult += `sortBy=${value.sortBy}`
    }
    state.sortQuery = queryResult
  }
}

const actions = {
  getFields({state, commit}, fieldsList) {
    return new Promise((resolve, reject) => {
      commit('setFields', fieldsList)
      Vue.axios.get(state.requestUrl.get)
        .then(res => {
          for (let field in state.fields) {
            state.fields[field].value = res.data[field]
            if (state.fields[field].hasOwnProperty('uploadState')) {
              state.fields[field].value ? state.fields[field].uploadState = 2
                : state.fields[field].uploadState = 0
            }
          }
          commit('setFieldsEditReady', true)
          resolve(res)
        }).catch(err => {
          reject(err)
          commit('setFieldsEditReady', false)
        })
    })
  },
  getContent({state, getters, commit}, {url, totalAmountUrl}) {
    return new Promise((resolve, reject) => {
      Vue.axios.get(`${url}${state.searchQuery}${getters.paginationQuery}${state.sortQuery}`)
        .then(res => {
          commit('setContentData', res.data)
        })
      if (totalAmountUrl) {
        Vue.axios.get(`${totalAmountUrl}${state.searchQuery}`)
          .then(res => {
            commit('setContentTotal', res.data.count)
          })
        resolve({contentData: state.contentData, contentTotal: state.contentTotal})
      }
      resolve({ contentData: state.contentData })
    })
  },
  postContent({ state, dispatch, getters }, { edit, parentId }) {
    return new Promise(async (resolve, reject) => {
      if (!getters.validateContent) {
        dispatch('launchNotification', {message: 'Заполни пустые поля'})
        dispatch('Sounds/playSound', { sound: 'error', stopSame: true, volume: .8 }, { root: true })
        reject('Empty fields')
      } else {
        if (edit) {
          Vue.axios.put(state.requestUrl.put, getters.fieldsForRequest({edit: true}))
          .then(res => {
            dispatch('launchNotification', {message: 'Запись изменена'})
            dispatch('Sounds/playSound', { sound: 'success', stopSame: true, volume: .8 }, { root: true })
            resolve(res)
          })
          .catch(err => {
            dispatch('processError', {error: err.response.data})
            reject(err.response.data)
          })
        } else {
          let randomGeneratedUrl = await dispatch('getRandomUrl', {
            length: 10,
            existCheckUrl: `${state.requestUrl.get}/url`
          })
          Vue.axios.post(state.requestUrl.post, getters.fieldsForRequest({randomGeneratedUrl}))
          .then(res => {
            dispatch('launchNotification', {message: 'Сделано'})
            dispatch('Sounds/playSound', { sound: 'success', stopSame: true, volume: .8 }, { root: true })
            resolve(res)
          })
          .catch(err => {
            dispatch('processError', {error: err.response.data})
            reject(err)
          })
        }
      }
    })
  },
  processError({state, commit, dispatch}, {error}) {
    if (error.includes('duplicate')) {
      dispatch('launchNotification', {message: 'Публикация с таким названием уже существует'})
      dispatch('Sounds/playSound', { sound: 'error', stopSame: true, volume: .8 }, { root: true })
    } else if (error.includes('Forbidden')) {
      dispatch('launchNotification', {message: 'Не авторизован'})
      dispatch('Sounds/playSound', { sound: 'error', stopSame: true, volume: .8 }, { root: true })
      router.push({name: 'login'})
    } else {
      dispatch('launchNotification', {message: 'Ошибка, браток'})
      dispatch('Sounds/playSound', { sound: 'error', stopSame: true, volume: .8 }, { root: true })
    }
  },
  setNestedFieldItem({state, commit, dispatch}, {data}) {
    let builtItem = {}
    let error = false
    if (data.edit) {
      for (let itemKey in state.fields[data.parentField].children) {
        if (!data.data[itemKey]) {
          error = true
        }
      }
      if (!error) {
        state.fields[data.parentField].value[data.edit.index] = data.data
        dispatch('Sounds/playSound', { sound: 'proceeded', stopSame: true, volume: .5 }, { root: true })
        commit('setNestedEditingNow', { type: 'remove' })
        commit('setNewNestedItem', { type: 'remove' })
      } else {
        dispatch('launchNotification', {message: 'Остались незаполненные поля'})
      }
    } else if (data.remove) {
      if (state.fields[data.parentField].value[data.remove.index]
        && state.nestedEditingNow.parentField !== data.parentField
        && state.nestedEditingNow.index !== data.remove.index) {
          state.fields[data.parentField].value.splice(data.remove.index, 1)
          dispatch('Sounds/playSound', { sound: 'proceeded', stopSame: true, volume: .5 }, { root: true })
      } else {
        dispatch('launchNotification', {message: 'Нельзя удалить в данный момент'})
      }
    } else {
      for (let itemKey in state.fields[data.parentField].children) {
        builtItem[itemKey] = data.hasOwnProperty('data') ? data.data[itemKey] || null : null
        if (!builtItem[itemKey]) {
          error = true
        }
      }
      if (!error) {
        state.fields[data.parentField].value.push(builtItem)
        dispatch('Sounds/playSound', { sound: 'proceeded', stopSame: true, volume: .5 }, { root: true })
        commit('setNewNestedItem', { type: 'remove' })
      } else {
        dispatch('launchNotification', {message: 'Остались незаполненные поля'})
      }
    }
  },
  launchNotification({ state, commit, dispatch }, { type, message, letterPause }) {
    if (state.notification.visible === false) {
      commit('setNotificationTimeout', 'clear')
      if (type === 'delete') {
        commit('setNotification', {message: '', visible: true, forDelete: true})
      } else {
        commit('setNotification', {message: '', visible: true})
      }

      let i = 0
      let letterInterval = letterPause || 100
      let animationDuration = (letterInterval * message.length) / 1000
      commit('setNotificationParam', { name: 'letterInterval', value: letterInterval })
      commit('setNotificationParam', { name: 'duration', value: animationDuration })
      function loop() {
        setTimeout(() => {
          commit('setNotificationMessage', state.notification.message + message[i])
          if (message[i] !== ' ')
            dispatch('Sounds/playSound', { sound: 'beepShort', stopSame: true, volume: .2 }, { root: true })
          if (i < message.length - 1) {
            i++
            loop()
          } else {
            commit('setNotificationParam', {name: 'isAnimPlaying', value: false})
            if (type !== 'delete') {
              setTimeout(() => {
                commit('setNotification', {message: '', visible: false})
              }, 1000)
            }
          }
        }, letterInterval)
      }
      setTimeout(() => {
        commit('setNotificationParam', {name: 'isAnimPlaying', value: true})
        loop()
      }, 700)
    }
  },
  setForDelete({ state, dispatch }, { requestUrl, itemId }) {
    state.forDelete = { requestUrl, itemId }
    dispatch('launchNotification', { type: 'delete', message: 'Удалить запись?'})
  },
  deleteApprove({ state, commit, dispatch }) {
    Vue.axios.delete(`${state.forDelete.requestUrl}/${state.forDelete.itemId}`)
    .then((res) => {
      commit('setNotificationParam', { name: 'visible', value: false })
      dispatch('launchNotification', { message: 'Сделано'})
      dispatch('getContent', {url: state.forDelete.requestUrl, totalAmountUrl: `${state.forDelete.requestUrl}/count` })
    })
  },
  getRandomUrl({state, commit, dispatch}, {length, existCheckUrl}) {
    return new Promise((resolve, reject) => {
      let result = ''
      let characters = state.charsForRandomUrl
      let charactersLength = characters.length
      function makeRandUrl() {
        for (let i = 0; i < length; i++) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        Vue.axios.get(`${existCheckUrl}/${result}`)
        .then(res => {
          if (res.data.hasOwnProperty('url')) {
            result = ''
            makeRandUrl()
          } else {
            resolve(result)
          }
        }).catch(err => {
          console.log(err.response)
          resolve(result)
        })
      }
      makeRandUrl()
    })
  }
}

const getters = {
  getNestedUploadState: state => keyField => {
    return state.newNestedItem.uploadState[keyField] || 0
  },
  paginationQuery: state => {
    let queryResult = state.searchQuery ? '&' : '?'
    let query = router.app._route.query
    let separator = ''
    let i = 0

    for (let queryParam in query) {
      if (i > 0) queryResult += '&'
      if (queryParam === 'page') {
        let offset
        if (query[queryParam] == 1) {
          offset = 0
        } else {
          offset = (Number(query[queryParam]) * state.pageItemLimit) - state.pageItemLimit
        }
        queryResult += `offset=${offset}`
        separator = '&'
      }
      i++
    }
    if (state.pageItemLimit) {
      queryResult += `${separator}limit=${state.pageItemLimit}`
    }
    return queryResult
  },
  getPagesCount: state => {
    let num
    if (state.contentTotal) {
        num = Math.ceil(state.contentTotal / state.pageItemLimit)
    }
    return num
  },
  filtersQuery: state => data => {

  },
  validateContent: state => {
    let haveErrors = false
    for (let field in state.fields) {
      if (!state.fields[field].value
        && field !== 'url'
        && state.fields[field].required) {
          state.fields[field].error = true
          haveErrors = true
      } else {
        if (field != 'parent_id') {
          if (state.fields[field].hasOwnProperty('validate')) {
            if (state.fields[field].validate === true) {
              state.fields[field].error = false
            } else {
              state.fields[field].error = true
              haveErrors = true
            }
          } else {
            state.fields[field].error = false
          }
        } else {
          if (!state.fields[field].value._id) {
            state.fields[field].error = true
            haveErrors = true
          } else {
            state.fields[field].error = false
          }
        }
      }
    }
    if (haveErrors) return false
    return true
  },
  fieldsForRequest: (state, dispatch) => ({edit, randomGeneratedUrl}) => {
    let newFields = {}
    if (state.requestUrl.parent_id) {
      newFields['parent_id'] = state.requestUrl.parent_id
    }
    if (state.requestUrl.content_id) {
      newFields['content_id'] = state.requestUrl.content_id
    }
    for (let field in state.fields) {
      if (field === 'url') {
        if (edit) {
          newFields[field] = state.fields[field].value
        } else {
          newFields[field] = randomGeneratedUrl
        }
      } else if (field != 'parent_id') {
        newFields[field] = state.fields[field].value
      }
    }
    return newFields
  }
}

export default {
  namespaced: true,
  state, mutations, actions, getters
}