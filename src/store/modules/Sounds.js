const state = {
  loadedSounds: {}
}

const mutations = {
  async initLoadedSounds(state, value) {
    let soundsInstance = {}
    for (let sound in value) {
      soundsInstance[sound] = {
        path: value[sound],
        audioInstance: await new Audio(value[sound])
      }
      soundsInstance[sound].audioInstance.volume = 0.3
    }
    state.loadedSounds = soundsInstance
  },
  async addSounds(state, value) {
    for (let sound in value) {
      state.loadedSounds[sound] = {
        path: value[sound],
        audioInstance: await new Audio(value[sound])
      }
    }
  }
}

const actions = {
  playSound({state}, {sound, stopSame, volume}) {
    if (state.loadedSounds.hasOwnProperty(sound)) {
      if (volume) state.loadedSounds[sound].audioInstance.volume = volume
      if (stopSame)  {
        state.loadedSounds[sound].audioInstance.pause()
        state.loadedSounds[sound].audioInstance.currentTime = 0.0
      }
      state.loadedSounds[sound].audioInstance.play()
    }
  }
}

export default {
  namespaced: true,
  state, mutations, actions
}
