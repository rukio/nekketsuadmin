import Vue from 'vue'
import VueAxios from 'vue-axios'

import axios from './axios'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo)
Vue.use(VueAxios, axios)