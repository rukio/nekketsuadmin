import store from '@/store'

import axios from 'axios'
import router from '@/router'

const instance = axios.create({
  baseURL: 'http://api.nst.rip',
  headers: {
    'Content-Type': 'application/json'
  }
})

instance.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem(store.state.tokenKeyName)

    if (token) {
      config.headers['Authorization'] = `Bearer ${ token }`
    }
    return config
  }, 

  (error) => {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  (config) => {
    return config
  }, 

  (error) => {
    if (error.response.status === 403) {
      localStorage.removeItem(store.state.tokenKeyName)
      router.push({ name: 'login' })
    }
    return Promise.reject(error)
  }
)

export default instance
